import java.util.Date;

import com.devcamp.models.Customer;
import com.devcamp.models.Visit;

public class App {
    public static void main(String[] args) throws Exception {
        Customer customer1 = new Customer("Son CHung");
        System.out.println("Customer1:");
        System.out.println(customer1);

        Customer customer2 = new Customer("Son Hai");
        System.out.println("Customer2:");
        System.out.println(customer2);

        Visit visit1 = new Visit(customer1, new Date());
        visit1.setProductExpense(20000);
        visit1.setServiceExpense(500000);
        System.out.println("Visit1:");
        System.out.println(visit1);

        Visit visit2 = new Visit(customer2, new Date());
        visit2.setProductExpense(40000);
        visit2.setServiceExpense(700000);
        System.out.println("Visit2:");
        System.out.println(visit2);

        System.out.println("Chi phí tổng của visit1:");
        System.out.println(visit1.getTotalExpense());
        System.out.println("Chi phí tổng của visit2:");
        System.out.println(visit2.getTotalExpense());
        
    }
}
